import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PlaneEditorComponent} from './admin/components/plane-editor/plane-editor.component';
import {PlaneListComponent} from './admin/components/plane-list/plane-list.component';


const routes: Routes = [
  {path: '', component: PlaneListComponent},
  {path: 'editor', component: PlaneEditorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

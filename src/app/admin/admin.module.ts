import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaneEditorComponent } from './components/plane-editor/plane-editor.component';
import {MatCardModule} from '@angular/material/card';
import { PlaneListComponent } from './components/plane-list/plane-list.component';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [PlaneEditorComponent, PlaneListComponent],
  exports: [
    PlaneEditorComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    FormsModule,
    MatButtonModule
  ]
})
export class AdminModule { }

import { Component, OnInit } from '@angular/core';
import {PlanesService} from '../../../services/planes.service';
import {Plane} from '../../../models/plane.model';

@Component({
  selector: 'app-plane-list',
  templateUrl: './plane-list.component.html',
  styleUrls: ['./plane-list.component.scss']
})
export class PlaneListComponent implements OnInit {

  text = 'hoho';
  planes: Plane[];

  constructor(private planesService: PlanesService) { }

  ngOnInit() {
    this.getPlanes();
  }

  getPlanes = () => this.planesService.getAllPlanes().then((planes: Plane[]) => this.planes = planes);

  showObj = (plane: Plane) => console.log('Plane ', plane);

}

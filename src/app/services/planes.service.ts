import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Plane} from '../models/plane.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlanesService {

  constructor(private http: HttpClient) {

  }

  public getAllPlanes(): Promise<Plane[]> {
    return this.http.get<Plane[]>(environment.apiUrl + '/api/airplanes/all_airplanes').toPromise();
  }


}

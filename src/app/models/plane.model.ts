export interface Plane {
  id: number;
  name: string;
  economicSeatsAmount: number;
  businessSeatsAmount: number;
  firstClassSeatsAmount: number;
}
